\begin{thebibliography}{10}

\bibitem{Bunke2011}
M.~Bunke and K.~Sohr.
\newblock {\em Engineering Secure Software and Systems: Third International
  Symposium, ESSoS 2011, Madrid, Spain, February 9-10, 2011. Proceedings},
  chapter An Architecture-Centric Approach to Detecting Security Patterns in
  Software, pages 156--166.
\newblock Springer Berlin Heidelberg, Berlin, Heidelberg, 2011.

\bibitem{Camilo:2015:BFV:2820518.2820551}
F.~Camilo, A.~Meneely, and M.~Nagappan.
\newblock Do bugs foreshadow vulnerabilities?: A study of the chromium project.
\newblock In {\em Proceedings of the 12th Working Conference on Mining Software
  Repositories}, MSR '15, pages 269--279, Piscataway, NJ, USA, 2015. IEEE
  Press.

\bibitem{DBLP:journals/ac/FeldererBJBBP16}
M.~Felderer, M.~B{\"{u}}chler, M.~Johns, A.~D. Brucker, R.~Breu, and
  A.~Pretschner.
\newblock Chapter one - security testing: {A} survey.
\newblock {\em Advances in Computers}, 101:1--51, 2016.

\bibitem{DBLP:journals/stvr/FeldererZBBP16}
M.~Felderer, P.~Zech, R.~Breu, M.~B{\"{u}}chler, and A.~Pretschner.
\newblock Model-based security testing: a taxonomy and systematic
  classification.
\newblock {\em Softw. Test., Verif. Reliab.}, 26(2):119--148, 2016.

\bibitem{Feng2016}
Q.~Feng, R.~Kazman, Y.~Cai, R.~Mo, and L.~Xiao.

\bibitem{DBLP:conf/msr/GarciaS14}
H.~V. Garcia and E.~Shihab.
\newblock Characterizing and predicting blocking bugs in open source projects.
\newblock In {\em 11th Working Conference on Mining Software Repositories,
  {MSR} 2014, Proceedings, May 31 - June 1, 2014, Hyderabad, India}, pages
  72--81, 2014.

\bibitem{DBLP:conf/msr/GegickRX10}
M.~Gegick, P.~Rotella, and T.~Xie.
\newblock Identifying security bug reports via text mining: An industrial case
  study.
\newblock In {\em Proceedings of the 7th International Working Conference on
  Mining Software Repositories, {MSR} 2010 (Co-located with ICSE), Cape Town,
  South Africa, May 2-3, 2010, Proceedings}, pages 11--20, 2010.

\bibitem{Haley08securityrequirements}
C.~B. Haley, R.~Laney, J.~D. Moffett, and B.~Nuseibeh.
\newblock Security requirements engineering: A framework for representation and
  analysis.
\newblock {\em IEEE TRANSACTIONS ON SOFTWARE ENGINEERING}, 34(1):133--153,
  2008.

\bibitem{Lucia:2012:FL:2664446.2664457}
Lucia, F.~Thung, D.~Lo, and L.~Jiang.
\newblock Are faults localizable?
\newblock In {\em Proceedings of the 9th IEEE Working Conference on Mining
  Software Repositories}, MSR '12, pages 74--77, Piscataway, NJ, USA, 2012.
  IEEE Press.

\bibitem{DBLP:journals/ese/MartinezM15}
M.~Martinez and M.~Monperrus.
\newblock Mining software repair models for reasoning on the search space of
  automated program fixing.
\newblock {\em Empirical Software Engineering}, 20(1):176--205, 2015.

\bibitem{Mitropoulos2012}
D.~Mitropoulos, G.~Gousios, and D.~Spinellis.
\newblock Measuring the occurrence of security-related bugs through software
  evolution.
\newblock In {\em Proceeding of the 16th Panhellenic Conference on
  Informatics}, PCI '12, pages 117 --122. IEEE Press, 2012.

\bibitem{DBLP:conf/icse/Monperrus14}
M.~Monperrus.
\newblock A critical review of "automatic patch generation learned from
  human-written patches": essay on the problem statement and the evaluation of
  automatic software repair.
\newblock In {\em 36th International Conference on Software Engineering, {ICSE}
  '14, Hyderabad, India - May 31 - June 07, 2014}, pages 234--242, 2014.

\bibitem{Murphy-Hill:2013:DBF:2486788.2486833}
E.~Murphy-Hill, T.~Zimmermann, C.~Bird, and N.~Nagappan.
\newblock The design of bug fixes.
\newblock In {\em Proceedings of the 2013 International Conference on Software
  Engineering}, ICSE '13, pages 332--341, Piscataway, NJ, USA, 2013. IEEE
  Press.

\bibitem{DBLP:journals/tse/Murphy-HillZBN15}
E.~R. Murphy{-}Hill, T.~Zimmermann, C.~Bird, and N.~Nagappan.
\newblock The design space of bug fixes and how developers navigate it.
\newblock {\em {IEEE} Trans. Software Eng.}, 41(1):65--81, 2015.

\bibitem{DBLP:conf/models/NguyenYHKST15}
P.~H. Nguyen, K.~Yskout, T.~Heyman, J.~Klein, R.~Scandariato, and Y.~L. Traon.
\newblock Sospa: {A} system of security design patterns for systematically
  engineering secure systems.
\newblock In {\em 18th {ACM/IEEE} International Conference on Model Driven
  Engineering Languages and Systems, MoDELS 2015, Ottawa, ON, Canada, September
  30 - October 2, 2015}, pages 246--255, 2015.

\bibitem{Pan:2009:TUB:1553586.1553603}
K.~Pan, S.~Kim, and E.~J. Whitehead, Jr.
\newblock Toward an understanding of bug fix patterns.
\newblock {\em Empirical Softw. Engg.}, 14(3):286--315, June 2009.

\bibitem{Park:2012:ESS:2664446.2664453}
J.~Park, M.~Kim, B.~Ray, and D.-H. Bae.
\newblock An empirical study of supplementary bug fixes.
\newblock In {\em Proceedings of the 9th IEEE Working Conference on Mining
  Software Repositories}, MSR '12, pages 40--49, Piscataway, NJ, USA, 2012.
  IEEE Press.

\bibitem{DBLP:conf/icse/SharTB13}
L.~K. Shar, H.~B.~K. Tan, and L.~C. Briand.
\newblock Mining {SQL} injection and cross site scripting vulnerabilities using
  hybrid program analysis.
\newblock In {\em 35th International Conference on Software Engineering, {ICSE}
  '13, San Francisco, CA, USA, May 18-26, 2013}, pages 642--651, 2013.

\bibitem{Smith:2015:QDA:2786805.2786812}
J.~Smith, B.~Johnson, E.~Murphy-Hill, B.~Chu, and H.~R. Lipford.
\newblock Questions developers ask while diagnosing potential security
  vulnerabilities with static analysis.
\newblock In {\em Proceedings of the 2015 10th Joint Meeting on Foundations of
  Software Engineering}, ESEC/FSE 2015, pages 248--259, New York, NY, USA,
  2015. ACM.

\bibitem{DBLP:journals/ese/TanLLWZZ14}
L.~Tan, C.~Liu, Z.~Li, X.~Wang, Y.~Zhou, and C.~Zhai.
\newblock Bug characteristics in open source software.
\newblock {\em Empirical Software Engineering}, 19(6):1665--1705, 2014.

\bibitem{DBLP:conf/issre/ThomeSB15}
J.~Thom{\'{e}}, L.~K. Shar, and L.~C. Briand.
\newblock Security slicing for auditing xml, xpath, and {SQL} injection
  vulnerabilities.
\newblock In {\em 26th {IEEE} International Symposium on Software Reliability
  Engineering, {ISSRE} 2015, Gaithersbury, MD, USA, November 2-5, 2015}, pages
  553--564, 2015.

\bibitem{Viega11}
J.~Viega and G.~McGraw.
\newblock {\em {Building Secure Software: How to Avoid Security Problems the
  Right Way}}.
\newblock Addison-Wesley, London, 1st edition, 2011.

\bibitem{Witschey:2015:QDA:2786805.2786816}
J.~Witschey, O.~Zielinska, A.~Welk, E.~Murphy-Hill, C.~Mayhorn, and
  T.~Zimmermann.
\newblock Quantifying developers' adoption of security tools.
\newblock In {\em Proceedings of the 2015 10th Joint Meeting on Foundations of
  Software Engineering}, ESEC/FSE 2015, pages 260--271, New York, NY, USA,
  2015. ACM.

\bibitem{DBLP:journals/infsof/XiaLSWY15}
X.~Xia, D.~Lo, E.~Shihab, X.~Wang, and X.~Yang.
\newblock Elblocker: Predicting blocking bugs with ensemble imbalance learning.
\newblock {\em Information {\&} Software Technology}, 61:93--106, 2015.

\bibitem{Yin:2011:FBB:2025113.2025121}
Z.~Yin, D.~Yuan, Y.~Zhou, S.~Pasupathy, and L.~Bairavasundaram.
\newblock How do fixes become bugs?
\newblock In {\em Proceedings of the 19th ACM SIGSOFT Symposium and the 13th
  European Conference on Foundations of Software Engineering}, ESEC/FSE '11,
  pages 26--36, New York, NY, USA, 2011. ACM.

\bibitem{DBLP:conf/sigsoft/YinYZPB11}
Z.~Yin, D.~Yuan, Y.~Zhou, S.~Pasupathy, and L.~N. Bairavasundaram.
\newblock How do fixes become bugs?
\newblock In {\em SIGSOFT/FSE'11 19th {ACM} {SIGSOFT} Symposium on the
  Foundations of Software Engineering {(FSE-19)} and ESEC'11: 13rd European
  Software Engineering Conference (ESEC-13), Szeged, Hungary, September 5-9,
  2011}, pages 26--36, 2011.

\bibitem{DBLP:conf/icse/YskoutSJ15}
K.~Yskout, R.~Scandariato, and W.~Joosen.
\newblock Do security patterns really help designers?
\newblock In {\em 37th {IEEE/ACM} International Conference on Software
  Engineering, {ICSE} 2015, Florence, Italy, May 16-24, 2015, Volume 1}, pages
  292--302, 2015.

\bibitem{Zaman:2011:SVP:1985441.1985457}
S.~Zaman, B.~Adams, and A.~E. Hassan.
\newblock Security versus performance bugs: A case study on firefox.
\newblock In {\em Proceedings of the 8th Working Conference on Mining Software
  Repositories}, MSR '11, pages 93--102, New York, NY, USA, 2011. ACM.

\bibitem{Zhong:2015:ESR:2818754.2818864}
H.~Zhong and Z.~Su.
\newblock An empirical study on real bug fixes.
\newblock In {\em Proceedings of the 37th International Conference on Software
  Engineering - Volume 1}, ICSE '15, pages 913--923, Piscataway, NJ, USA, 2015.
  IEEE Press.

\bibitem{DBLP:conf/icse/ZimmermannNGM12}
T.~Zimmermann, N.~Nagappan, P.~J. Guo, and B.~Murphy.
\newblock Characterizing and predicting which bugs get reopened.
\newblock In {\em 34th International Conference on Software Engineering, {ICSE}
  2012, June 2-9, 2012, Zurich, Switzerland}, pages 1074--1083, 2012.

\end{thebibliography}
