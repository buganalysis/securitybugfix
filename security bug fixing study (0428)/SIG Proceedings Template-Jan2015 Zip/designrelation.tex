In this paper, we focused on why security bugs are  difficult to resolve and how they are fixed based on the fix patterns and fix process of security bugs.

\textbf{Security Bug Fixing Patterns}

First, we found that for security bug fixes, most of the fixes are small, which is different other types of bugs~\cite{Zaman:2011:SVP:1985441.1985457,Witschey:2015:QDA:2786805.2786816}. As pointed out
by Monperrus~\cite{DBLP:journals/ese/MartinezM15,DBLP:conf/icse/Monperrus14}, existing approaches are effective in fixing
this type of bugs (fixed with simple fix changes). Moreover, the results  reveal the
essential operators to fix security bugs. If we compare these operators
with an existing approach, we may estimate the potential of
the approach for fixing security bugs. In addition, in our study, the most common fix patterns for security bugs are CKD (check data by adding IF module, assertion module and mark variable) and ACB (Add code branch by adding IF/SWITCH module). This finding is different from the findings in \cite{Witschey:2015:QDA:2786805.2786816}, which shows that MC-DAP (method call with different
actual parameter values), IF-CC (change in if conditional), and
AS-CE (change of assignment expression) are the most fix patterns in the bug fix changes. Hence, for different types of bugs, their fix patterns may be different.  Hence, when proposing or developing automatic program repair techniques or tools for security bug fixing, it is necessary to consider the special fix patterns in security bugs.

\textbf{Blocking for Security Bugs}

In addition, some security bugs needs a long time to fix since they have blocking bugs. To fix security bugs, developers not only need to fix security related blocking bugs, but also to spend an amount time on non-security related blocking bugs. From our study, we notice that non-security related blocking bugs are more than the security related bugs, which shows that during the process of fixing security bugs, developers not only need to focus on the security problems, but also to concern the relation among the platform, functionality and security. Some work have focused on characterizing and predicting blocking bugs \cite{DBLP:conf/msr/GarciaS14,DBLP:journals/infsof/XiaLSWY15}, and shows that  bug comments, the number of developers
in the CC list, the bug reporter, etc.  are important indicators for blocking bugs prediction. But for security bugs, there are different blocking relations, as we identified three for non-security related and security related bugs. So when using existing work to predict the blocking bugs for security bugs, some different or more factors may be considered.

\textbf{Process of Security Bug Fixing}

In our study, we found that more than 80\%   security bugs needs multiple commits, which is  different from existing findings, i.e., Park et al. reported that only a  portion of resolved bugs (22\% in Eclipse JDT core, 24\% in Eclipse SWT, and 33\% in Mozilla) involve supplementary patches with multiple commits. So for security bugs, they are more difficult to fix and still need concerns after their fixes. In addition, we summarize five different procedures in multi commits for security bugs. Among these, Process \uppercase\expandafter{\romannumeral1} (Fix->Test) and Process \uppercase\expandafter{\romannumeral2} (Fix->Better Fix) are more widely used. This shows that for security bugs, a lot of tests need to be done later to verify the original fixes, and some better fixes may be used instead of original fixes. So  security-related  regression testing  is important  for security bug fixes. However, most of existing security testing techniques focused on testing security in the development phase, while quite few on security-related  regression testing. Hence, both academics and industries need to study or develop security-related regression testing techniques or tools to help developers verify the security fixes.


\textbf{Reopens of Security Bugs}

During the process of bug fixing, some bugs may not be fully fixed and need to be reopened \cite{DBLP:conf/sigsoft/YinYZPB11}. In our study, we identified right causes for security bug reopens.   Among these causes, \textbf{Type \uppercase\expandafter{\romannumeral1}} (coordinate relation with the original fix) is the most.  This indicates that when fixing security bugs, program analysis or clone detection tools are needed to help identify such coordination relation related to the security bug in the program. In addition, we also notice that there are some security bugs that are reopened, but developers did not do any substantial fixes, instead, just some tests  \textbf{Type \uppercase\expandafter{\romannumeral7}} or manual check  \textbf{Type \uppercase\expandafter{\romannumeral8}}. So when applying existing bug reopen  prediction techniques for security bugs \cite{DBLP:conf/icse/ZimmermannNGM12,DBLP:journals/infsof/XiaLSWY15}, they can consider the special factors for these two reopen causes to decrease the false positives of the prediction. In this way, developers can devote more to those security bugs needing substantial fixes.

\textbf{Design Problems in Security Bug Fixing}

During the process of our study, we found that there are some design problems that make the security bug fixes more difficult. For example, a source file contains a large amount of miscellaneous functions which are unrelated, which leads to some unstable files in the system. Once these files were changed, the relevant files need to be changed together, which complicates the fix. In addition,   some fixes of the security also incur the regression of the design because there was an architecture defect in the system. So the fix may aggravate  such defect, which also induces the bug reopens (for example, the bug 320982\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=320982$}).

From our study, we notice that most of the fixes for security bugs were small. Developers usually  just add patches to wherever the bugs are without considering the total design of the project. For example, we list some examples of the design problem in security bug fixing.

\begin{itemize}
  \item \textbf{Duplicate code} Duplicate code in security bug fixes is very common. The same  code fixes may act in different parts of the program, which causes the duplicate code. For example, for bug 360293\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=360293$},  some float variables must be checked before using them. So developers usually add check to wherever has float passing to  fix  this bug. This causes the duplicate code of those float variables' check. Suppose we move all the float variables' check in a place and all float passing should call it and get the float, then the float check can just be implemented in one place which is good to protect, test and maintain. But this may take time to consider where the float check should be and to replace all float passing with this call. But in practice,  fixing security bug is urgent and developers have little time to consider these problems. If there are some clone detection tools to do this, the quality of security bug fixes may be improved.
  \item \textbf{Useless code} During the process of fixing security bugs, some code becomes useless. These codes should be removed because they may have some potential security problems that may be used by attackers.
  \item \textbf{God class} If there is a god class in the program, it may be difficult to identify where the security problem is. This may cause more time and cost in security bug fixing. For example, for bug326501\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=326501$},  it reported that the old tree widget is quite complex and "nasty" because it implements many things  for performance reasons. These things are hard to implement and would make things even more complex.  
  \item \textbf{Coupling between modules} For bug321299\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=321299$}, its fix involved both C++ file and CSS file, where there is a coupling between them. Such bad coupling  can cause some problems during the fixing process, such as hard to identify all the fix positions, which may lead to bug reopens. So loose coupling between different modules can alleviate the difficulty of security bug fixing and avoid some of their reopens.
%  \item \textbf{Refactoring of fixes} In the process of security bug fixing, we find that there is a process that is to first fix the security bugs, and then refactor the original fixes.  Although such procedure occurs not so frequent, but it can effectively improve the quality of the bug fixing.

\end{itemize}


During the process of fixing security bugs, if developers can consider  the design,  the efficiency and quality of security bug fixing process may be improved (rather than reopens or supplementary fixes).

