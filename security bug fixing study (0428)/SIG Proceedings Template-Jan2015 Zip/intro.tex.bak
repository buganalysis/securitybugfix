Security bugs are one of the primary concerns for software developers during
software maintenance and evolution~\cite{DBLP:journals/ese/TanLLWZZ14,Haley08securityrequirements,Viega11}.  A bug is referred to as security related when it creates
vulnerability in the software, which the malicious attackers could exploit to
attack the system~\cite{Viega11}. As we know, security vulnerabilities are a   threat to any organization, which can  cause
serious monetary and reputation damage.  Early fix of security bugs in
software artifacts can help   reduce the potential damages to finances and trust.

Security bugs exist widely in released software.  Security bugs may not have been reported as often as other types of bugs such as functional bugs, because they do not cause fail-stop failures. However,  it is time to pay more attention to them when we enter the
human-centered computing world. Mozilla has a special community for security bug management\footnote{https://wiki.mozilla.org/Security}, and offers up to \$10,000 to security bug hunters. It is essential and emergent to fix secure bugs when they are reported.  Zaman et al. reported that security bugs are triaged and fixed
fastest ~\cite{Zaman:2011:SVP:1985441.1985457}. However,   Zaman et al. also found that   security bugs are the most  reopened and tossed compared to other types of bugs ~\cite{Zaman:2011:SVP:1985441.1985457}. In addition, Mitropoulos et al.    found that the number
of unresolved security bugs are increasing in the projects compared to the
other bugs~\cite{Mitropoulos2012}. These results show  that security bug fixes are relatively more difficult.

 Therefore, both the research  and industry have
spent great effort on addressing security bugs. For example,
many techniques are proposed recently to identify security bugs~\cite{Zaman:2011:SVP:1985441.1985457}, predict  vulnerable components~\cite{DBLP:conf/issre/ThomeSB15,DBLP:conf/icse/SharTB13,DBLP:conf/msr/GegickRX10}, usage of security patterns~\cite{DBLP:conf/models/NguyenYHKST15,DBLP:conf/icse/YskoutSJ15}, security testing~\cite{DBLP:journals/stvr/FeldererZBBP16,DBLP:journals/ac/FeldererBJBBP16}, etc. However, quite a few of studies focused on the   security bug fixes.   The lack of empirical studies on topics like ``how security bugs are fixed
by developers'', ``what the blocking relations  are  to block security bugs from being fixed'',  and ``what the common root causes of reopened and multi-committed security bugs are'',   have severely limited the design of security bug avoidance, testing,   and fixing techniques and tools.

To fill this gap,  this paper focuses on a deep study on the problems related to security bug fixes. To the best of our knowledge, it is the first comprehensive study of real-world security bug fixes based on  the bug databases of  the Mozilla project. Research on these problems can guide the design of techniques and tools for addressing
security bug fixes in the following ways:

\begin{itemize}
  \item 358  of security bugs were committed with simple fix patterns in a single commit. In our study, $10$ automatically extractable security bug fix patterns are identified using the
syntax components and context of the source code involved in bug fix changes. These identified fix patterns for security bugs  will provide insights on the potential of automatic program repair for fixing the security bugs. For example, the results will reveal
how many bugs can be fixed by simple changes, i.e., the
essential operators to fix bugs. If we compare these operators
with an existing approach, we may estimate the potential of
the approach for fixing bugs.
  \item In addition to the single commit of security bug fixes, there are 363 security bugs that are intentionally resolved win multiple commits (multi-committed security bugs). These multi-committed security bug fixes may have their own context or complexity. The process of multi-committed security bug fixes can guide developers how to effectively deal with these special security bugs.
  \item One reason for a long-time  security bug fix is due to its blocking bugs. Blocking bugs are software bugs that prevent other bugs from being fixed. These blocking bugs can lengthen the overall fixing time, reduce overall quality and delay the release of the software systems. The block relation for security bugs include non-security blocking and security blocking bugs. We identify ... These can help identify the blocking security bugs, improve the accuracy of existing blocking bug prediction, and ultimately enable earlier fixes of security bugs.
  \item There are 68 security bugs that are reopened in our study. Reopened bugs increase software
maintenance cost, cause rework for already busy developers, which also
indicates instability in the software system.  We identified nine reasons for reopened  security bugs,  which can help improve existing techniques on   prediction of reopened bugs or guide developers to make similar mistakes in bug reopens.
  %\item The relation between design and security bug fixes ...
\end{itemize}

The rest of the paper is organized as follows: Section 2 introduces the study design of our study. Sections 3 -5 present  the finding of our empirical study. Section 6 discusses the related work of empirical study of security bugs and bug fixes. Finally, we
present our conclusion and future work in Section 7.
