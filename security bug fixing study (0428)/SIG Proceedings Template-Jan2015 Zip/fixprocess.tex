
To fix the security bugs, there are usually some blocking bugs that prevent them being fixed. So when a security bug needs to be resolved, its blocking bugs need to be first fixed. For different security bugs, developers have different ways to fix them. For example, for simple security bugs, it can be resolved in a simple commit. But for some more complex security bugs, they may be resolved in multiple commits. If a security bug is not fully fixed or fixed in an incorrect way, it may be reopened. In this section, we focus on the fixing process of security bugs from three aspects, blocking relations, fix process, and the fix impacts.


\subsection{Block relation in security bug fixes}
When a security bug needs to be fixed, developers should first fix its blocking bugs as they prevent the security bugs from being fixed. These blocking bugs include both non-security bugs and security related bugs. In our study, we aim to identify how these security bugs depend on their blocking bugs.

\begin{figure}
\centering
\includegraphics[width=8cm]{block.jpg}
\caption{Blocking relation for security bugs} \label{block}
\end{figure}


\begin{table*}
\begin{center} \caption{Blocking bugs for different types of security bugs}
\begin{tabular}{l|c|c||c|c|c}
\hline    &  \multicolumn{2}{|c||}{ \textbf{Security blocking bugs}} &     \multicolumn{3}{c}{ \textbf{Non-security blocking bugs}}\\ \cline{2-6}
   \raisebox{1.5ex}{\textbf{Security Bug}}  & Block \uppercase\expandafter{\romannumeral1}  &  Block \uppercase\expandafter{\romannumeral3}  &  Block \uppercase\expandafter{\romannumeral1}  &  Block \uppercase\expandafter{\romannumeral2}  &  Block \uppercase\expandafter{\romannumeral3} \\ \hline

   Information leakage (5)  & 0&	0	&1	& 	1	 &	0
 \\
 \hline

 DOS (3)   &  0	&0	&1	&1	 &	0	
 \\
  \hline
  XSS(35)  &   2	&3	&4	&5	&4	
 \\
        \hline

         Buffer overflow (52)  &  0	 & 3	 & 4 & 	7	 &4	
 \\
   \hline

      Memory leak (8)   &   0 & 	1	& 1	& 2	&   0	
\\
   \hline

    Using none-owned memory (92)   &   0 &	1	&13	&10	&5	

\\
   \hline
   Others (5)   &   0	&0	&0	&0	&0

\\
   \hline
   \hline
   Sum &   2	&8	&24	&26	&13	

\\
   \hline


\end{tabular}
\end{center}
\end{table*}

Based on the analysis of the blocking relation in the Mozilla project, we find that there are 65 security bugs that have 63 non-security bugs and 10 security bugs. For these two different blocking bugs, we identify different blocking relations as shown in Figure \ref{block}. These blocking bugs can be divided into three types, In-functionality block (Block \uppercase\expandafter{\romannumeral1}), Cross-functionality block (Block \uppercase\expandafter{\romannumeral2}), and Platform-block (Block \uppercase\expandafter{\romannumeral3}. In-functionality block represents the blocking bugs within a functionality. For example, the issue of bug 350312\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=350312$} is that a   wrong value is popped from the stack with nested catch/finally module. However, there are some problems within the catch/finally module, which becomes the blocking bugs, as reported in bug 350837\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=350837$}. When fixing a security bug, if the fix patches depend on some other components within the same functionality, which also have non-security or security bugs, these bugs become the in-functionality blocking bugs. Cross-functionality block means  the blocking bugs between different   functionalities, which occurs only for non-security bugs. For example, Bug 656277\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=656277$} reports that the WebGL component cannot normally load images. But WebGL component  applying cross-domain textures to load images also has problems, as reported by bug 662599\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=662599$}.  So this cross-domain textures forms a cross-functionality blocking bug, which should be first fixed.    For platform-block, it means that there are a bug in the platform that may prevent its associated components (with security bugs) from being fixed. The platform-block includes both non-security related bugs and security related bugs. For example, the issue of bug 662309 \footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=662309$} is that if a .jar file is installed, users are forced to download the arbitrary files, which may cause information leaks. But to patch this security bug, the problem of \emph{Download Manager frame} should be first resolved, which is reported by its blocking bug 657462\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=657462$}.



A summary of different types of blocking bugs corresponding to different security bugs is shown in Table 2. From the results, we notice that non-security bugs are the main blocking bugs that prevent security bugs from being fixed. In addition, Block \uppercase\expandafter{\romannumeral1} (In-functionality block) and Block \uppercase\expandafter{\romannumeral2} (Cross-functionality block) in non-security blocking are the most happened blocking bugs. Moreover,  Block \uppercase\expandafter{\romannumeral2} (Cross-functionality block) only happens in non-security blocking bugs.

\subsection{Process of security bug fixes}

In our study, we find that different security bugs are fixed in different ways, i.e., some security bugs are fixed in a commit while others need multiple commits. For 856 security bugs in our study, 712 security bugs were committed in multiple times, and 134 committed once. For these 134 single commit, we find that these security bugs  are usually very simple, only one or several simple fix patterns can  be used to fix them.

\begin{figure}
\centering
\includegraphics[width=9cm]{fixprocess.jpg}
\caption{Fix process of security bugs} \label{fixprocess}
\end{figure}



But for  complex security bugs, they need several commits. We summarize the process of multiple fixes into five types, as shown in Figure \ref{fixprocess}. The first type of process is a widely used fix process (Process \uppercase\expandafter{\romannumeral1}), i.e., developers first fix  a security bug and commit  it, then they may verify whether their fixes  are correct. So they test their fixes and commit again (for example, for bug 679494\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=679494$}). Since fixing security bugs is usually emergent, developers may employ a temporary fix on the security bug. When he/she has enough time, a better fix pattern may be used to refix the original security bug  (Process \uppercase\expandafter{\romannumeral2}).  For example, for the issue of information leakage of bug 664009\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=664009$},  the developer  cannot find a good fixing method, so they try to patch it in a simple way. Later he/she find some better patches, and  recommitted the better fixes again.  In addition, there are also some complex security bugs, which is difficult to fix in a single commit. At this time, developers may divide the fixes into several sub-fixes with multiple fixes  (Process \uppercase\expandafter{\romannumeral3}). For example, the issue of bug 389580\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=389580$} reported that on windows XP some urls for  web  protocols that contain '\%00' launch the wrong handler and appear to be able to launch local programs. The developer  first   fix some external URLs containing '\%00' in a commit, then  more patches were done to avoid major changes by checking URI through the newer CreateUri class.  There are  also some fixes that are not correct in its initial commit. Then the developers need to fix the original security bug again  (Process \uppercase\expandafter{\romannumeral4}), for example, bug 685186\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=685186$}. To improve the quality of fixes, developers sometimes refactor their previous fixes of security bugs, which is  another typical fix process in multiple commits  (Process \uppercase\expandafter{\romannumeral5}). For example, to fix bug 653238\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=653238$},  the developer modified many  files and caused a lot of other bugs, so the developer then refactored the code to alleviate this problem.

\begin{table*} %\scriptsize
\begin{center}
\caption{Fix process of multi-commits for different types of security bugs}
%\begin{tabularx}{6cm}{l|l|l}
\begin{tabularx}{12.5cm}{l|c|c|c|c|c}
\hline
  \textbf{Security bug}  &  \textbf{Process \uppercase\expandafter{\romannumeral1}} &  \textbf{Process \uppercase\expandafter{\romannumeral2}} &  \textbf{Process \uppercase\expandafter{\romannumeral3}} &  \textbf{Process \uppercase\expandafter{\romannumeral4}} &  \textbf{Process \uppercase\expandafter{\romannumeral5}}  \\\hline
   Information leakage (5) & 2&3	&0	&1&	0	
 \\
 \hline

  DOS (3)   & 2&0	&1	&1&	0
 \\
  \hline
  XSS(35)  & 12&13	&7	&5&	1
 \\
        \hline

Buffer overflow (52)   & 17&20	&3	&6&	0
 \\
   \hline

      Memory leak (8)  & 1&3	&0	&2&	1
\\
   \hline

    Using none-owned memory (92)   & 54&16	&7	&8&	2


\\
   \hline
   Others (5)  & 2&1	&0	&1&	0

\\
   \hline
   \hline
   Sum &   90	&56	&18	&24	&4	

\\
   \hline

\end{tabularx}
\end{center}
\end{table*}

The statistics of the process of multiple commits for different security bugs is shown in Table 3. We notice that Process \uppercase\expandafter{\romannumeral1} (fix->test) is the most used in multi-commits of security bugs, followed by Process \uppercase\expandafter{\romannumeral2} (fix->better fix). There are also some incorrect fixes (i.e., 24 security bugs) for security bugs, which need to be refixed as Process \uppercase\expandafter{\romannumeral3} shows.

\subsection{Impact of security bug fixes}

During the fix process of the security bugs, some may be not fully resolved. Then these security bugs needed to be reopened and refixed. In this section, we aim to find the causes of these security bugs that are    reopened.


\begin{table*} %\scriptsize
\begin{center}
\caption{Causes for reopened security bugs for different types of security bugs}
%\begin{tabularx}{6cm}{l|l|l}
\begin{tabularx}{16cm}{l|c|c|c|c|c|c|c|c}
\hline
  \textbf{Security bug}  &  \textbf{Type \uppercase\expandafter{\romannumeral1}} &  \textbf{Type \uppercase\expandafter{\romannumeral2}} &  \textbf{Type \uppercase\expandafter{\romannumeral3}} &  \textbf{Type \uppercase\expandafter{\romannumeral4}} &  \textbf{Type \uppercase\expandafter{\romannumeral5}}  &  \textbf{Type \uppercase\expandafter{\romannumeral6}} &  \textbf{Type \uppercase\expandafter{\romannumeral7}} &  \textbf{Type \uppercase\expandafter{\romannumeral8}}\\\hline
   Information leakage (5)  & 0&1	&0	&0&	0 &0	&0&	0	
 \\
 \hline

  DOS (3)   & 0&0	&0	&0&	0 &0	&0&	0
 \\
  \hline
 XSS(35)  & 7&0	&0	&2&	3 &4	&3&	0
 \\
        \hline

Buffer overflow (52)   & 2&0	&0	&3&	0 &2	&2&	3
 \\
   \hline

      Memory leak (8)   &0&1	&0	&1&	0 &2	&0&	0
\\
   \hline

   Using none-owned memory (92)  & 6&0	&2	&1&	0&0	&2&	2


\\
   \hline
   Others (5)  & 1&0	&1	&0&	0 &1	&0&	1

\\
   \hline
   \hline
   Sum &   16	&2	&4	&6	&3	&9 & 7 & 6

\\
   \hline

\end{tabularx}
\end{center}
\end{table*}
%
%\begin{table*} %\scriptsize
%\begin{center}
%\caption{Causes for reopened security bugs}
%%\begin{tabularx}{6cm}{l|l|l}
%\begin{tabularx}{14cm}{l|c|l|c}
%\hline
%  \textbf{Category}  &  \textbf{Sub-category} &  \textbf{Type ID} &  \textbf{Frequency} \\\hline
%  Fix the problem  related to the original security bug & coordinate relation & Type \uppercase\expandafter{\romannumeral1} & 16 \\
%    &  Linear relation & Type \uppercase\expandafter{\romannumeral2} & 1 \\
% \hline
%
%  Enhance the fixes of the original security bug &  Be wider& Type \uppercase\expandafter{\romannumeral3} & 4 \\
%    &   Be stronger & Type \uppercase\expandafter{\romannumeral4}  &6 \\
%  \hline
%  Relax the  fixes of the original security bug &   & Type \uppercase\expandafter{\romannumeral5} & 3 \\
%        \hline
%
%         Revise the  useless   fixes of the original security bug &   & Type \uppercase\expandafter{\romannumeral6} & 9 \\
%   \hline
%
%      Check or add tests for the  original fixes of the security bug &   & Type \uppercase\expandafter{\romannumeral7} & 6\\
%   \hline
%
%    Suspect the  original fixes of the security bug but do nothing &   & Type \uppercase\expandafter{\romannumeral8} & 8\\
%   \hline
%
%\end{tabularx}
%\end{center}
%\end{table*}

There are in total 68 reopened security bugs in our study. However, only 54 of these reopened security bugs have commit information. Among these 54 reopened security bugs, 42 of them reopen once, 11   twice, and the rest   three times. After studying these bugs, we identify six causes for reopened security bugs, which is shown in Table 1. There are six different causes that leads to the reopens of the security bugs. For some causes, they can be further categorized by the context of different security bugs. To fix the reopened security bugs, they can be fixed in different ways, as shown in Figure \ref{reopenfix}.


\begin{figure}
\centering
\includegraphics[width=9cm]{reopen.jpg}
\caption{Fixes for reopened security bugs} \label{reopenfix}
\end{figure}

First, some security bugs are reopened because they are not completely fixed in its original fixes. So this type of bug reopen is used to fix the problem related to the original security bug. The relation between the reopened bug and its original bug include coordinate relation (Type \uppercase\expandafter{\romannumeral1}) and liner relation (Type \uppercase\expandafter{\romannumeral2}). For example, bug 389106\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=389106$},  it is the problem of escape URIs (especially quotes) when passing them to external protocol handlers. There are many escaped characters need to handle, so in the first fix, they handle some necessary characters except space. However, space also needs to be handled, and this bug was reopened. In this example, space and other characters are the similar problem at the same level, and belongs to the coordinate relation as shown in Figure \ref{reopenfix}. In addition, there is also a linear relation between the reopened bug and the original security bug. For example, for bug 312278\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=312278$}, it happened for access of GC-ed object in Array.prototype.toSource, some objects are GC-ed before it can be used. The first fix is to root the objects that may be GC-ed before used. But some objects may be GC-ed before it can be rooted, so reopen to root those objects more early. Reopened fix is beyond the first fix in a logic line of this problem. So it is linear elation.

There are also some fixes for the reopened bugs to enhance the fixes of the original security bug. Two ways can be used to enhance the original security bugs, i.e., Be wider (Type \uppercase\expandafter{\romannumeral3}) and be stronger (Type \uppercase\expandafter{\romannumeral4}). For example, bug 452979\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=452979$} reports that invisible control characters in URL must not be decoded when showing its address. The fix is to encode the characters  shown in the url. The original fix just encoded some characters but not all of them. Then this bug was reopened  to encode the rest characters. The fix for this reopend bug is to expand its protection scope, as shown in Figure \ref{reopenfix}. For the way of being stronger, we can refer to the bug 351236\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=351236$}, which reported a crash   with designMode iframes, removing styles, removing iframes, reloading, etc. In this bug, the original fix is to wallpaper the problem instead of really fixing it. Then, the bug is reopened  to find a more perfect   solution to fully fix it.

To fix some security bugs, sometimes the fix is too strong, which may affect the performance or functionality of the system. So some security bugs are reopend to relax the  fixes of the original security bug (Type \uppercase\expandafter{\romannumeral5}). For example,   bug 418356\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=418356$} reported that it is unsafe to use mozIJSSubScriptLoader.loadSubScript() with non-chrome urls or chrome urls whose scheme/host part contain uppercase characters. The original fix is to set the right url in the script and do not allow loading non-chrome scripts. But later users found   that some scripts are needed for the system with url file, i.e.,\//. So the bug was reopened to re-enable loading file  using the subscript loader.

There are also some security bugs that are not correctly revised, which needs to be reopened for refixing (Type \uppercase\expandafter{\romannumeral6}). For bug 522430\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=522430$}, which reported that window.opener allows chrome access from unprivileged pages. A developer is assigned to fix  this bug. However,   the users found   that the   fix still not solved the problem, so this bug was reopened for resolution.

In our study, we found that there are some fixes for security bug reopens without any changes on the code. For these security bug reopens, developers just added some tests  (Type \uppercase\expandafter{\romannumeral7}) or checked the original fixes (Type \uppercase\expandafter{\romannumeral8}). For example, for bug 331679\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=331679$}, it reopened just  for stress test. For bug 336409\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=336409$}, which reported   an integer overflow in js\_obj\_toSource. However,   the program still crashed in some tests, so the developer thought that the original fix was wrong and reopened it. But it turned out that the crash is not caused by the original fix or this security bug.


Table 4 shows the causes of different security bug reopens and their frequencies. From the results, we notice that fixing of Type \uppercase\expandafter{\romannumeral1} (coordinate relation) reopened security bugs is the most in our study, followed by the Type \uppercase\expandafter{\romannumeral6} (revise the originally useless fixes of a  security bug) reopened security bugs. In addition, we notice that for XSS security bug, although there are only 35 XSS security bugs in our study, it has the most number of reopens, which is more than the using none-owned memory (we have 92 samples in our study).



%\begin{figure}
%\centering
%\includegraphics[width=10cm]{multicommitreason.jpg}
%\caption{Causes for multi-committed security bugs} \label{multicommitreason}
%\end{figure}


