

Security bug is a type of  vulnerabilities that are   related to software, which is a software bug that can be exploited to gain unauthorized access or privileges on a computer system.  The Open-Source Vulnerability Database (OSVDB\footnote{http://osvdb.org/}), classifies software vulnerabilities into six categories. Considering both this category and the security analysis on Mozilla, we classify   the security bugs in our study into seven types: Buffer
Overflow, Cross-Site Scripting (XSS),   Denial
of Service (DoS), Information Leakage, Memory Leak,  
Using None-owned Memory and others that cannot be classified using the above category. Among these different types of security bugs, information leakage, memory leak and
using none-owned memory   are  used based on the analysis of the security bugs from Mozilla.  Information leakage represents that some information is leaked to unauthorized parties. Memory leak is a type of resource leak that occurs when a computer program incorrectly manages memory allocations, such as assigning NULL value to a variable.  Using none-owned memory happens in the context of using pointers to access and modify memory. If such a pointer is a null pointer, dangling pointer (pointing to memory that has already been freed), or to a memory location outside of current stack or heap bounds, it indicates that the memory   is not  possessed by the program.  Then accessing such memory can cause  operating system exceptions, leading to a program crash. The statistic  of these different types of security bugs is shown in the first column (with its frequency in the bracket) in Table 1, which show that using none-owned memory, buffer overflow and XSS are the most in our study.

\begin{figure*}
\centering
\includegraphics[width=15cm]{fixpatterns.jpg}
\caption{Common fix patterns of security bugs} \label{fixpattern}
\end{figure*}



To resolve a security bug, fixes need to be conducted on the software. First, we aim to examine if there are any typical fix patterns to resolve security bugs. We manually analyzed  the security bug fix change
history of Mozilla and found  10 different fix patterns, which is classified into three groups from the perspective of data computation process in a code module, as shown in Figure \ref{fixpattern}.
Typically there are three procedures for data computation to fix security bugs,  data preprocessing, data processing and data ensuring. These three procedures play  different roles in the security  of data computation, and have their respective corresponding fix patterns.



For data preprocessing, it is usually used to prevent the external attacks on the data, which is generally set in the entrance of a code module. There are four different fix patterns for data preprocessing, i.e.,  check data, filter illegal data, strengthen data constraint and weaken data constraint.

\begin{itemize}
  \item Check data (\emph{CKD}): Checking the legality of data is necessary for security usage of the data before processing it. The simplest way to check data is adding an \emph{if module}, i.e., if the data  conforms to the condition, it will be passed into the code module. In addition,  a code module needs to use data from other modules. Sometimes, some illegal data may be  transmitted from other  code modules, which may cause code crash. In this way, an assertion may be used to verify its  correctness  before using the data.
  \item Filter illegal data (\emph{FID}): For a code module, some illegal or malicious data is delivered   to attack the system. So these  illegal or malicious data  need to be filtered out to prevent them from 	contaminating the code module. A typical  fix pattern is to add an \emph{ if module}  to check the legality of data as the above \emph{CKD} fix pattern. The difference  is that \emph{FID} fix pattern needs to filter the illegal data. When the data is stored in a   table   or a tree, a \emph{loop module }  needs to be used for iteration over the data.
  \item Strengthen data constraint (\emph{SDC}): When adding an if module to check the legality of data, not all the features of the legal data is met. In this way, some illegal data can  be still used within the code module, which leads to  secure problems, such as memory leak or code crash. For example, the issue of bug 610601\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=610601$} is that firefox will intermittent crash because sometimes the data in \emph{mImageData} object is null, so the if module condition should be strengthened to check whether the frame has the image data before handling the image. Then,   the conditions of the original if module should be strengthened to consider this problem. In addition, there is another  \emph{SDC} fix pattern, which  is used to move some preliminary treatment code into the if module, as this module is mainly to preprocess legal data, such as converting data format. Moving the preliminary treatment code into if module is to make sure only the legal data is preprocessed.
  \item Weaken data constraint (\emph{WDC}): Contrary to WDC, some legal data may    be filtered out from a code module if the module conditions are too strong to tackle security issues. So   fixes are needed to weaken the if condition or move some preliminary treatment code out of the if module. %For example, the issue  650001\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=650001$}   is that the entities with namespaceID aren't re-encoded by the innerHTML getter which when parsed by the second innerHTML of course leads to the XSS. That is, the entities with namespaceID are also the legal data for the encode code module, so the patch of bug 650001 is to weaken the conditions of if module, which primarily increase the legal data characteristics of the entities with namespaceID, when filtering the illegal data.
\end{itemize}

For data processing, it is used to guarantee that the data is processed in a secure way.   So some  fixe patterns are used to guarantee the correct data usage during the data processing for security problems. There are four different fix patterns for data  processing, i.e.,  add code branch, change the type of an object   to parent class, change the type of an object to son class and, add tryCatch module.


\begin{itemize}
  \item Add code branch (\emph{ACB}): For some data, code may need a different approach to reach a different expected purpose. So the condition  branch, such as if/switch module, should be extended for the corresponding process. For example, the problem of bug 675747\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=675747$}  is that when the second nsOggCodecState object has been created and added to the mCodecState object, the original seiral-to-nsOggCodecState mapping in mCoedcStates has been overwritten which leads to code crash. So an if branch has been added to distinguish the context of adding the two nsOggCodecState objects to avoid overwriting.
  \item Change the type of an object to its parent class (\emph{COP}): We assume that  two classes (\emph{C1} and \emph{C2}) are inherited from the class \emph{C3}, and the defensive code protects the variables of C1 from attacking, but the variables of C2 are attacked by the malicious code. The simplest way to patch this bug is to change the variable \emph{C1} to parent type \emph{C3}. %The primary issue of bug 653926\footnote{$https://bugzilla.mozilla.org/show_bug.cgi?id=653926$}   is that updating the version of mozIJSSubScriptLoader object from 3.6 to 4.0 leads to some inconsistent behaviors which cause some parameters no longer being the scope objects and cannot be wrapped that may be leaked or destroyed. The patch for this bug is to change the wrapping principal (mSystemPrincipal) to the parent class nsIPrincipal and add some other handling code, so that the wrapping scope can be extended and all the parameters can be wrapped.
  \item Change the type of an object to its son class (\emph{COS}): CVP can help extend the defending scope, but it can also lead to some other secure problems. For example, if the code module is only used to handle the data in variables of \emph{C1}, but the actual type of the variables is \emph{C3}, then the data of \emph{C2} may also be mis-transferred into the module, which can cause memory overflow or other secure issues. For this situation, the variable of \emph{C3} may need to be changed to its son class \emph{C1}.
%\end{itemize}
%
%For the fix patterns of CVP and CVS, we can apply ACB to replace them, but the fix complexity of ACB should be higher a lot. Moreover, ACB can lead more inconsistent fix if replaced for CVP, because developers need to handle the same/similar steps for the two son classes, which is the architectural regression of code design and not benefit for the further system maintenance. So we can summarize the priorities of CVP and CVS are higher than ACB.
%
%
%\begin{itemize}
  \item Add tryCatch module (\emph{ATC}): ATC is   used here is to   catch errors of the data when it is incorrect for some security problems.   ATC is usually used to handle some light error, which can restore the current operating environment.
  \item Change the weak reference to strong reference (\emph{CWS}): A weak reference is a reference that does not protect the referenced object from collection by a garbage collector, unlike a strong reference. So CWS is   used here to protect the referenced object from being collected.
%  \item Use SecurityManager object (\emph{USM}): SecurityManager is a class which specifically protects the data, which stored in it, from attacked. SecurityManager is like a closed box, all the external malicious programs cannot invade, which may be a good secure design to protect the data from destroyed.
\end{itemize}

For data ensuring in the fix process,  it is used to insure the validity and the legality of the results of the code module before transmitting them to the other code module(s). The primary fix pattern is also the check data (\emph{CKD-E}) pattern similar to that of \emph{CKD} in the data preprocessing.

\begin{itemize}
  \item Check data (\emph{CKD-E}): The data may not be correctly computed in the part of data process, so   the validity of the results of the data computation needs to be checked. A typical check way is to add if module.
\end{itemize}


\begin{table*} %\scriptsize
\begin{center}
\caption{Different types of security bugs and their fix patterns}
%\begin{tabularx}{6cm}{l|l|l}
\begin{tabularx}{15cm}{l|c|c|c|c|c|c|c|c|c|c}
\hline
  \textbf{Security bug (frequency)}  &  \textbf{\emph{CKD}} &  \textbf{\emph{FID}} &  \textbf{\emph{SDC}} &  \textbf{\emph{WDC}} &  \textbf{\emph{ACB}} &  \textbf{\emph{COP}} &  \textbf{\emph{COS}} &  \textbf{\emph{ATC}} &  \textbf{\emph{CWS}} &  \textbf{\emph{CKD-E}}\\\hline
  Information leakage (5) & 4&	0	&0	&0&	1	&2&	0&	0	&0	&	2
 \\
 \hline

  DOS (3) &  2	&1	&0	&0	&1&	0	&0&	0	&0&	0
 \\
  \hline
  XSS(35) &   24	&1	&5	&7	&19	&0	&0	&1&	0	&7
 \\
        \hline

         Buffer overflow (37) &  20	 & 7	 & 4 & 	1	 & 21	 & 1 & 	0	 & 0	 & 0 & 	1
 \\
   \hline

      Memory leak (8) &   7 & 	0	& 3	& 0	& 5	& 0	& 0	& 0& 	0	& 0
\\
   \hline

    Using none-owned memory (92) &   52 &	8	&4	&7	&43	&0	&0	&1	&25	& 13
\\
   \hline
  Stack overflow (15) &   9	& 1	& 1	& 1	& 8	& 1	& 1	& 1	& 0	& 3

\\
   \hline
   Others (5) &   0	&0	&0	&0	&2	&0	&1	&0	&0	&1

\\
   \hline
   \hline
   Sum &   118	&18	&17	&16	&98	&4	&2	&3	&25	&27

\\
   \hline

\end{tabularx}
\end{center}
\end{table*}

For different types of security bugs, they may use different fix patterns as discussed above.  A summary of fix patterns corresponding to different types of security bugs is shown in Table 1. From Table 1, we notice that   CKD (check data) and ACB (add code branch) are the most two widely used fix patterns for security bugs. In addition, for different fix patterns, they are used to resolve different security bugs. For example, the \emph{CWS} (change the weak reference to strong reference) fix pattern is solely used for resolve the type of using none-owned memory bugs. For the \emph{FID} (filter illegal data) fix pattern, it is mostly used to resolve buffer overflow and using none-owned memory security bugs; for \emph{WDC} (weaken data constraint), it is widely used to resolve XSS and using none-owned memory security bugs; and for \emph{CKD-E} (Check data for data ensuring), it is widely used to resolve  using none-owned memory security bugs.




